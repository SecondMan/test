
## <a name="source-comp"></a>Source Compositions ##

With the release of KartaVR v3.5 all macro nodes are delivered as GroupOperators so you can expand the node icon in the Fusion flow area and adjust any setting inside of the macro.

Fusion Macros are saved as ".setting" files and they can be opened and viewed in a plain text editor like Notepad++ or TextWrangler. This is handy if you want to change the default input scaling resolution. The only issue with editing a .setting file is that it can be hard to re-flow how the compositing node tree is connected since it is difficult to visualize the input and output connections.

If you want to visually edit/rework how the macros function, each of the original source Fusion ".comp" files that were used to create the KartaVR are accessible.

### Windows Source Compositions ###

`C:\Program Files\KartaVR\macros\KartaVR\Source Compositions\`

or Fusion 7 Macros Folder:

`C:\Users\Public\Documents\Blackmagic Design\Fusion\Macros\KartaVR\Source Compositions\`

or Fusion 8 Macros Folder:

`%appdata%\Blackmagic Design\Fusion\Macros\KartaVR\Source Compositions\`

### macOS Source Compositions ###

`/Applications/KartaVR/macros/KartaVR/Source Compositions/`

or Fusion 8 Macros Folder:

`/Users/<User Account>/Library/Application Support/Blackmagic Design/Fusion/Macros/KartaVR/Source Compositions/`

### Linux Source Compositions ###

`/opt/KartaVR/macros/KartaVR/Source Compositions/`

or Fusion 8 Macros Folder:

`$HOME/.fusion/BlackmagicDesign/Fusion/Macros/KartaVR/Macros/KartaVR/Source Compositions/`

### Macro Screenshot ###

![Editing a Macro](images/sample-source-composition.png)

