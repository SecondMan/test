<html>
	<head>
		<style>
			a {
				color: rgb(139,155,216);
			}
		</style>
	</head>
	<body>
		<h1>KartaVR VR Hypertext Compositing Guide</h1>

		<p>Welcome to a quick VR tutorial presented using a SBS sidecar webpage.</p>

		<h2>Overview</h2>
		
		<p>When you complete this exercise you will have a Fusion node graph that looks roughly like this:</p>
		
		<p><img src="Comp:/Docs/0_nodes.png"></p>
		
		<p>The output from the composite is a stitched 360&deg;x180&deg; LatLong formatted panoramic image of a stream flowing underneath a bridge.</p>
		
		<p><img src="Comp:/Docs/stitched_panorama.png"></p>

		<h2>Prerequisties</h2>
		
		<p>You need to have the KartaVR atoms installed. You can do this by <a href="AddAtom://">opening the Reactor package manager</a>.</p>
		
		<p>Switch to the KartaVR category and select the main "KartaVR" package that allows you to install all of the dependencies at once. Click the "install" button. A few minutes later everthing will be installed and ready to go.</p>
		
		<p><img src="Comp:/Docs/reactor_install_kartavr.png"></p>

		<h2>Project Assets</h2>
		
		<p>The "Under the Bridge Stitching" project includes three fisheye photos in the "Media" folder:</p>
		
		<p><img src="Comp:/Docs/media_assets.png"></p>
		
		<p>These images were captured using a single Sony A7Sii 4K full frame video camera, a Peleng 8mm circular fisheye lens, a Nodal Ninja 3 panorama head with the RD16-II Advanced Rotator, and an Atomos 4K Ninja Flame SSD based video recorder.</p>
		
		<p><img src="Comp:/Docs/camera.png"></p>

		<h1>Building the Comp</h1>

		<h2>Part 1 - Masking the Views</h2>

		<p>The "Under-the-Bridge-Stitching.comp" project starts with three Loader nodes that have been renamed. These nodes are called "<a href="View://CameraA">CameraA</a>", "<a href="View://CameraB">CameraB</a>", and "<a href="View://CameraC">CameraC</a>".</p>
		
		<p><i>(Click on the blue hyperlinks in the previous paragraph to view each of the nodes in Fusion's Viewer window.)</i><p>
		
		<p><img src="Comp:/Docs/1_start_nodes.png"></p>
		
		<p>These Loader nodes are used to access the "CameraA.0000.jpg", "CameraB.0000.jpg", and "CameraC.0000.jpg" fisheye views from the <a href="file:///Comp:/Media/">project's "Media" folder</a>.</p>
		
		<p>The first step to prepare these fisheye views for stitching is to mask out the content outside the center circular part of the image. This removes the area outside the active photographic image data such as the border of the lens.</p>
		
		<p><a href="Select://">Deselect all of the nodes</a> in the comp.</p>
		
		<p>Let's <a href="AddSetting://Reactor:/Deploy/Macros/KartaVR/Mask/FisheyeCropMask.setting">add a new "FisheyeCropMask" macro node</a> to the composite. This node is provided by KartaVR's masking tools.</p>
		
		<p><img src="Comp:/Docs/2_fisheyecropmask_node.png"></p>
		
		<p>The FisheyeCropMask node is used as an EffectMask on footage. It helps to apply a circular masking effect to images that have sensor cropping that cut into the idealized 1:1 aspect ratio circular fisheye frame.<p>
		
		<p>If your camera records a 4:3, or a 16:9 ratio image (like 1080p HD/4K UHD video cameras do) then the FisheyeCropMask node is a time saver since it can bring in the sides of the mask with a rectangular shaped "Crop Border Width" control to help feather the edge.<p>
		
		<p><img src="Comp:/Docs/2_fisheyecropmask.png"></p>
		
		<p><a href="View://FisheyeCropMask">Select the FisheyeCropMask node</a>, and in the Inspector window set the "Width" and "Height" values to match the resolution of our Loader node based footage. In this case the "Width" should be set to "2700" px. And the "Height" should be set to "2700" px.
		
		<p><img src="Comp:/Docs/3_fisheyecropmask_width_height.png"></p>
		
		<p>This updated 2700 x 2700 px image size gives the FisheyeCropMask a 1:1 aspect ratio.</p>
		
		<p><img src="Comp:/Docs/4_fisheyecropmask_square_ar.png"></p>
		
		<p>Let's connect the FisheyeCropMask node output to the EffectsMask inputs on the CameraA, CameraB, CameraC nodes so we can use the viewer window to visually adjust the remaining settings on the FisheyeCropMask node.</p>
		
		<p><img src="Comp:/Docs/5_fisheyecropmask_node_connected.png"></p>
		
		<p><a href="View://CameraB">Select the CameraB node and load it into the Viewer</a> window. The FisheyeCropMask has clipped out the center area of the image in a perfectly circular shape. The area outside this zone is now transparent.</p>
		
		<p><img src="Comp:/Docs/5_camerab_circular_masked.png"></p>
		
		<p>While still viewing the output of the CameraB node in the viewer window, <a href="Select://FisheyeCropMask">let's select the FisheyeCropMask node in the Nodes view</a> to adjust its settings in the inspector window.</p>
		
		<p>This results in the viewer window showing the image data from the CameraB node, while drawing the masking controls as a red overlay from the FisheyeCropMask node.<p>
		
		<p>We need to expand the size of the circular masking shape to reveal more of the fisheye image data. To do this, let's set the FisheyeCropMask node's "Scale X" and "Scale Y" controls to "0.95".</p>
		
		<p><img src="Comp:/Docs/6_fisheyecropmask_scale_xy.png"></p>
		
		<p>The red circular overlay in the viewer window now is much closer to the border of the frame. An issue exists though, where thin vertical black bars are now visible on the left and right side of the image.</p>
		
		<p><img src="Comp:/Docs/6_camerab_circular_masked_scale_095.png"></p>
		
		<p>We can bring in the edges of the cropping circle region by setting the FisheyeCropMask node's "Crop Border Width" control to "-0.15".</p>
		
		<p><img src="Comp:/Docs/7_fisheyecropmask_crop_border_width_015.png"></p>
		
		<p>To rotate the cropping edge from a horizontal crop, to a vertical cropping effect, set the "Angle" control to "90".</p>
		
		<p><img src="Comp:/Docs/7_fisheyecropmask_angle_90.png"></p>
		
		<p>And finally to soften the mask's corner edge where the cropped sides meet the circular radius zone, set the "Blur" control to "4".</p>
		
		<p><img src="Comp:/Docs/7_fisheyecropmask_blur_4.png"></p>
		
		<p>If we take a peek and <a href="View://FisheyeCropMask">view the "FisheyeCropMask" node in the viewer directly</a> this is what the finished mask looks like:</p>
		
		<p><img src="Comp:/Docs/7_fisheyecropmask_crop_border_width_mask.png"></p>
		
		<p>And this is what the result looks like when we <a href="View://CameraB">look at the CameraB Loader node</a> with all the masking settings applied.</p>
		
		<p><img src="Comp:/Docs/7_fisheyecropmask_blur_4_result.png"></p>
		
		<p>We can <a href="PassthroughOn://FisheyeCropMask">set the "Passthrough" state to ON for the FisheyeCropMask node</a> to bypass this node in the comp so we can quickly view the original Loader node media without the EffectsMask input's contribution.</p>
		
		<p><img src="Comp:/Docs/7_fisheyecropmask_passthrough_on.png"></p>
		
		<p>The hotkey for the changing the selected node's Passthrough state is "Command+P" on macOS, and "Control+P" on Win/Linux.</p>
		
		<p>When you toggle the passthrough state to ON, the shading on the node takes on a darker tint and it is deactivated in the comp. When you turn the passthrough mode back to its default OFF state the node shape regains its normal brightness in the Nodes view and the node's output is active again in renderings.</p>
		
		<p><img src="Comp:/Docs/7_passthrough_states.png"></p>
		
		<p>Note: <i>Before we continue further in the tutorial, we have to make sure to <a href="PassthroughOff://FisheyeCropMask">set the "Passthrough" state back to OFF again for the FisheyeCropMask node</a>. Otherwise we won't have the masking active for the remaining steps.</i></p>
		
		<p>At this point in the tutorial, let's individually view each of the "<a href="View://CameraA">CameraA</a>", "<a href="View://CameraB">CameraB</a>", and "<a href="View://CameraC">CameraC</a>" Loader nodes to see what each of the views look like with the vector masking applied.</p>
		
		<h2>Part 2 - Warping the Fisheye Footage</h2>
		
		<p>Now we are ready to start the fun part which is warping the fisheye footage. This stage is where the panoramic 360&deg; stitching magic really starts to happen infront of your eyes!</p>
		
		<p>KartaVR has a "Fisheye2Equirectangular" macro node that allows us to warp a circular fisheye image into an equirectangular image projection.<p>
		
		<p>Note: <i>An equirectangular projection is also called a LatLong (Latitude/Longitude), or spherical projection. It has 360&deg;x180&deg; image coverage. This format is the most popular way to present 2D and stereoscopic 3D 360VR media on the web today with video platforms like YouTube 360.</i></p>
		
		<p>Let's <a href="Select://CameraA">select the CameraA node</a> in our comp.</p>
		
		<p>Next we need to <a href="AddSetting://Reactor:/Deploy/Macros/KartaVR/Conversion/Fisheye2Equirectangular.setting">add a new "Fisheye2Equirectangular" macro node</a>. This node is provided by KartaVR's Conversion macros.</p>
		
		<p>Since we had the CameraA node selected beforehand, when the Fisheye2Equirectangular node is created and placed in our composite, it will be connected automatically to the CameraA node's output connection.</p>
		
		<p>This is what our composite should look like in Fusion's Nodes view area.</p>
		
		<p><img src="Comp:/Docs/8_fisheye2equirectangular_node.png"></p>
		
		<p>Let's <a href="View://Fisheye2Equirectangular">view the new Fisheye2Equirectangular node in our comp</a> to see its default output in the viewer window.</p>
		
		<p><img src="Comp:/Docs/8_fisheye2equirectangular_default_setting.png"></p>
		
		<p>When the Fisheye2Equirectangular node is added, the "FOV" (Field of View) starts at "180&deg;", and the "Yaw/Pitch/Roll" settings are all set to "0&deg;" rotation. The "Height" value defaults to "1920" px which generates a 3840x1920 px panoramic image that has a native a 2:1 aspect ratio.</p>
		
		<p><img src="Comp:/Docs/8_fisheye2equirectangular_inspector.png"></p>
		
	</body>
</html>